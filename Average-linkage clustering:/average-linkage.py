import numpy as np

def pairwise_distances(X):
    """Compute pairwise distances between points in a matrix X."""
    n = X.shape[0]
    distances = np.zeros((n, n))
    for i in range(n):
        for j in range(i+1, n):
            distances[i, j] = np.sqrt(np.sum((X[i] - X[j])**2))
            distances[j, i] = distances[i, j]
    return distances

def average_linkage_clustering(X, k):
    """Perform hierarchical clustering using average linkage."""
    n = X.shape[0]
    distances = pairwise_distances(X)
    clusters = [{i} for i in range(n)]
    while len(clusters) > k:
        # Find the closest pair of clusters
        min_distance = np.inf
        for i in range(len(clusters)):
            for j in range(i+1, len(clusters)):
                distance = np.mean([distances[a, b] for a in clusters[i] for b in clusters[j]])
                if distance < min_distance:
                    min_distance = distance
                    min_i, min_j = i, j
        # Merge the closest pair of clusters
        clusters[min_i] |= clusters[min_j]
        del clusters[min_j]
    return clusters
